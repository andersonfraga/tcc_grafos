package examples;

import examples.classes.AdjListGraph;
import examples.classes.AdjMatrixGraph;
import examples.classes.AdjMatrixGraphDirected;
import examples.classes.AdjMatrixWeightedGraph;
import examples.classes.GrafoDirRotMatAdj;
import graphRT.classes.GraphRT;
import graphRT.classes.SugiyamaLayout;
import graphRT.classes.SparseLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Main {

	public static void example1() {


		GraphRT grt = new GraphRT();
		AdjMatrixGraph gMatrix = new AdjMatrixGraph(4);
		grt.setGraph(gMatrix);

		gMatrix.addEdge(0, 1);
		gMatrix.addEdge(0, 2);
		gMatrix.addEdge(1, 2);
		gMatrix.addEdge(2, 0);
		gMatrix.addEdge(2, 3);

		grt.plot(new SugiyamaLayout());


	}

	public static void example2() {
		GraphRT grt = new GraphRT();
		AdjListGraph gList = new AdjListGraph(4);
		grt.setGraph(gList);

		gList.addEdge(0, 1);
		gList.addEdge(0, 2);
		gList.addEdge(1, 2);
		gList.addEdge(2, 3);

		grt.plot(new SugiyamaLayout());
	}

	public static void example3() {

		GraphRT grt = new GraphRT();
		AdjMatrixWeightedGraph gMatrix = new AdjMatrixWeightedGraph(4);
		grt.setGraph(gMatrix);

		gMatrix.addEdge(0, 1, 5);
		gMatrix.addEdge(0, 2, 3);
		gMatrix.addEdge(1, 2, 2);
		gMatrix.addEdge(2, 0, 6);
		gMatrix.addEdge(2, 3, 7);

		grt.plot(new SugiyamaLayout());
	}

	public static void example4() throws Exception {
		GraphRT grt = new GraphRT();

		GrafoDirRotMatAdj<String, Integer> mapa = new GrafoDirRotMatAdj<>(20);
		grt.setGraph(mapa);


		mapa.addNode("Ijui");
		mapa.addNode("POA");
		mapa.addNode("Viamão");
		mapa.addNode("Cidreira");
		mapa.addNode("Canoas");
		mapa.addNode("Pelotas");
		mapa.addNode("Palmeira");
		mapa.addNode("Passo Fundo");
		mapa.addNode("NH");
		mapa.addNode("Esteio");
		mapa.addNode("Sapucaia");
		mapa.addNode("Sapiranga");
		mapa.addNode("Rio Grande");
		mapa.addNode("Santa Cruz");
		mapa.addNode("Santa Maria");
		mapa.addNode("Vacaria");
		mapa.addNode("Santa Rosa");
		mapa.addNode("Tupanciretã");
		mapa.addNode("Itaqui");
		mapa.addNode("Guaíba");


		mapa.addEdge("Ijui", "Viamão", 96);
		mapa.addEdge("Ijui", "Esteio", 83);
		mapa.addEdge("Viamão", "Palmeira", 33);
		mapa.addEdge("Viamão", "Itaqui", 78);
		mapa.addEdge("Viamão", "Pelotas", 60);
		mapa.addEdge("Esteio", "Pelotas", 77);
		mapa.addEdge("Esteio", "Itaqui", 76);
		mapa.addEdge("Esteio", "NH", 84);
		mapa.addEdge("Rio Grande", "Guaíba", 5);
		mapa.addEdge("Rio Grande", "Viamão", 34);
		mapa.addEdge("Rio Grande", "Cidreira", 40);
		mapa.addEdge("Santa Maria", "Santa Cruz", 87);
		mapa.addEdge("Santa Cruz", "Guaíba", 19);
		mapa.addEdge("Santa Cruz", "Passo Fundo", 10);
		mapa.addEdge("Sapiranga", "Sapucaia", 74);
		mapa.addEdge("Sapiranga", "NH", 38);
		
		grt.plot(new SugiyamaLayout());
		Thread.sleep(3000);

		mapa.addEdge("Guaíba", "Sapiranga", 60);
		mapa.addEdge("Itaqui", "Sapiranga", 27);
		mapa.addEdge("Itaqui", "Cidreira", 13);

		mapa.addEdge("Passo Fundo", "Canoas", 20);
		mapa.addEdge("Passo Fundo", "Cidreira", 78);

		mapa.addEdge("POA", "NH", 18);
		mapa.addEdge("POA", "Palmeira", 94);
		mapa.addEdge("POA", "Santa Cruz", 50);
		
		grt.plot(new SugiyamaLayout());
		Thread.sleep(3000);

		mapa.addEdge("Pelotas", "Itaqui", 18);
		mapa.addEdge("Tupanciretã", "Pelotas", 42);
		mapa.addEdge("Tupanciretã", "Sapucaia", 41);
		mapa.addEdge("Guaíba", "Tupanciretã", 21);

		mapa.addEdge("Itaqui", "Passo Fundo", 66);
		mapa.addEdge("Vacaria", "Canoas", 47);


		grt.plot(new SugiyamaLayout());
	}

	public static void example5() {

		GraphRT grt = new GraphRT();
		GrafoDirRotMatAdj<String, Integer> mapa = new GrafoDirRotMatAdj<String, Integer>(5);
		grt.setGraph(mapa);

		mapa.addNode("POA");
		mapa.addNode("Viamão");
		mapa.addNode("Cidreira");
		mapa.addNode("Canoas");
		mapa.addNode("Pelotas");

		mapa.addEdge("POA", "Viamão", 30);
		mapa.addEdge("POA", "Canoas", 30);
		mapa.addEdge("Canoas", "Cidreira", 20);
		mapa.addEdge("Canoas", "Pelotas", 20);

		grt.plot(new SugiyamaLayout());

	}

	public static void example6() {
		GraphRT grt = new GraphRT();
		AdjMatrixGraphDirected gMatrix = new AdjMatrixGraphDirected(4);
		grt.setGraph(gMatrix);

		gMatrix.addEdge(0, 1);
		gMatrix.addEdge(0, 2);
		gMatrix.addEdge(1, 2);
		gMatrix.addEdge(2, 3);

		grt.plot(new SparseLayout());
	}

	public static void main(String[] args) throws Exception {

		example5();

	}

}
