package examples.classes;

/* source: https://www.programiz.com/dsa/graph-adjacency-list */

import java.util.*;

import graphRT.annotations.EdgeList;

public class AdjListGraph {
	private int numVertices;
	private LinkedList<Integer> adjLists[];

	public AdjListGraph(int vertices) {
		numVertices = vertices;
		adjLists = new LinkedList[vertices];

		for (int i = 0; i < vertices; i++)
			adjLists[i] = new LinkedList();
	}

	public void addEdge(int src, int dest) {
		adjLists[src].add(dest);
	}
	
	@EdgeList(weighted=false, directed = false)
	public LinkedList<Integer>[] getAdjLists() {
		return adjLists;
	}

}
