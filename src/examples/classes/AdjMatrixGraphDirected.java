package examples.classes;
import graphRT.annotations.EdgeMatrix;

/* source: https://www.programiz.com/dsa/graph-adjacency-matrix */

public class AdjMatrixGraphDirected {
	private boolean adjMatrix[][];
	
	private int numVertices;

	public AdjMatrixGraphDirected(int numVertices) {
		this.numVertices = numVertices;
		adjMatrix = new boolean[numVertices][numVertices];
	}

	public void addEdge(int i, int j) {
		adjMatrix[i][j] = true;
	}

	public void removeEdge(int i, int j) {
		adjMatrix[i][j] = false;
	}

	public boolean isEdge(int i, int j) {
		return adjMatrix[i][j];
	}
	
	@EdgeMatrix(weighted=false, directed = true)
	public boolean[][] getAdjMatrix() {
		return adjMatrix;
	}

	/**
	 * @return the numVertices
	 */
	public int getNumVertices() {
		return numVertices;
	}

	/**
	 * @param numVertices the numVertices to set
	 */
	public void setNumVertices(int numVertices) {
		this.numVertices = numVertices;
	}
}
