package examples.classes;
import graphRT.annotations.EdgeMatrix;

/* source: https://www.programiz.com/dsa/graph-adjacency-matrix */

public class AdjMatrixWeightedGraph {
	private int adjMatrix[][];
	
	private int numVertices;

	public AdjMatrixWeightedGraph(int numVertices) {
		this.numVertices = numVertices;
		adjMatrix = new int[numVertices][numVertices];
	}

	public void addEdge(int i, int j, int weight) {
		adjMatrix[i][j] = weight;
		adjMatrix[j][i] = weight;
	}

	public void removeEdge(int i, int j) {
		adjMatrix[i][j] = 0;
		adjMatrix[j][i] = 0;
	}

	public boolean isEdge(int i, int j) {
		return adjMatrix[i][j] > 0;
	}
	
	@EdgeMatrix(weighted=true, directed = false)
	public int[][] getAdjMatrix() {
		return adjMatrix;
	}

	/**
	 * @return the numVertices
	 */
	public int getNumVertices() {
		return numVertices;
	}

	/**
	 * @param numVertices the numVertices to set
	 */
	public void setNumVertices(int numVertices) {
		this.numVertices = numVertices;
	}
}
