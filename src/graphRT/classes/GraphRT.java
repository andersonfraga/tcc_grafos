package graphRT.classes;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import graphRT.annotations.EdgeMatrix;
import graphRT.annotations.VertexList;

public class GraphRT {
	// Main object
	private Object graph;

	// Graph structure
	private Object graphData;
	private List vertexList;
	private Class nodeClass;
	private Method nodeMethod;

	// Graph options
	private GraphType type;
	private boolean directed;
	private boolean weighted;

	// Nodes & Edges
	ArrayList<GraphRTNode> nodes = new ArrayList();
	ArrayList<GraphRTEdge> edges = new ArrayList();

	public GraphRT() {}

	public GraphRT(Object graph) {
		setGraph(graph);
	}

	public Object getGraph() {
		return graph;
	}

	public void setGraph(Object graph) {
		this.graph = graph;
		checkGraphType();
	}

	public GraphType getType() {
		return type;
	}

	public void setType(GraphType type) {
		this.type = type;
	}

	public boolean isDirected() {
		return directed;
	}

	public void setDirected(boolean directed) {
		this.directed = directed;
	}

	public boolean isWeighted() {
		return weighted;
	}

	public void setWeighted(boolean weighted) {
		this.weighted = weighted;
	}

	public Object getGraphData() {
		return graphData;
	}

	public void setGraphData(Object graphData) {
		this.graphData = graphData;
	}

	// Helpers

	private void clearGraph() {
		nodes.clear();
		edges.clear();
	}


	// Graph type checking

	private void checkGraphType() {
		Class aClass = this.graph.getClass();
		for (Method method : aClass.getDeclaredMethods()) {
			for (Annotation ann : method.getDeclaredAnnotations()) {
				Class<? extends Annotation> annType = ann.annotationType();
				boolean isFieldGraphData = false;

				// Verifies if it has the annotation is EdgeMatrix
				if (annType.getSimpleName().equals("EdgeMatrix")) {
					EdgeMatrix annMatrix = method.getAnnotation(EdgeMatrix.class);
					isFieldGraphData = true;
					this.type = GraphType.ADJMATRIX;
					setWeighted(annMatrix.weighted());
					setDirected(annMatrix.directed());
					System.out.println("This is an adj matrix graph\n");
				}

				// Verifies if it has the annotation is EdgeList
				if (annType.getSimpleName().equals("EdgeList")) {
					isFieldGraphData = true;
					this.type = GraphType.ADJLIST;
					System.out.println("This is an adj list graph");
				}


				// Verifies if it has the annotation is VertexList
				if (annType.getSimpleName().equals("VertexList")) {
					VertexList annVertexList = method.getAnnotation(VertexList.class);

					// get nodeClass
					this.nodeClass = annVertexList.nodeClass();
					for (Method nodeMethod : this.nodeClass.getDeclaredMethods()) {
						for (Annotation nodeAnn : nodeMethod.getDeclaredAnnotations()) {
							Class<? extends Annotation> annNodeType = nodeAnn.annotationType();
							if (annNodeType.getSimpleName().equals("Vertex")) {
								// save annotated node method
								this.nodeMethod = nodeMethod;
								this.nodeMethod.setAccessible(true);
								break;
							}
						}
					}

					try {
						// Get vertex list data invoking the method which is annotated
						method.setAccessible(true);
						this.vertexList = (List) method.invoke(this.graph);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else if (isFieldGraphData) {
					try {
						// Get graph data invoking the method which is annotated
						method.setAccessible(true);
						this.graphData = method.invoke(this.graph);
					} catch (Exception e) {
						e.printStackTrace();
					}

				}

			}
		}
	}

	private GraphRT2D getLayout(GraphRTLayout layout)
	{
		return new GraphRT2D(layout);
	}

	// Graph view

	private void updateGraph(GraphRT graphRT, GraphRTLayout layout) {
		clearGraph();

		if (graphRT.getType() == GraphType.ADJMATRIX) {
			updateAdjMatrixGraph(graphRT);
		} else if (graphRT.getType() == GraphType.ADJLIST) {
			updateAdjListGraph(graphRT);
		}

		getLayout(layout).updatePaint(nodes, edges, directed);
	}
	// Nodes view

	private void updateGraph(GraphRT graphRT, GraphRTLayout layout, int index, int level) {
		clearGraph();

		if (graphRT.getType() == GraphType.ADJMATRIX) {
			updateAdjMatrixNodes(graphRT, index, level);
		} else if (graphRT.getType() == GraphType.ADJLIST) {
			updateAdjListNodes(graphRT, index, level);
		}

		getLayout(layout).updatePaint(nodes, edges, directed);
	}

	// Graph view - Matrix

	private void updateAdjMatrixGraph(GraphRT graphRT) {
		if (graphRT.getGraphData() instanceof boolean[][]) {
			updateAdjMatrixGraph((boolean[][]) graphRT.getGraphData(), graphRT.isDirected());
		} else if (graphRT.getGraphData() instanceof int[][]) {
			updateAdjMatrixGraph((int[][]) graphRT.getGraphData(), graphRT.isDirected());
		} else {
			updateAdjMatrixGraph((Object[][]) graphRT.getGraphData(), graphRT.isDirected());
		}
	}

	private void updateAdjMatrixGraph(boolean[][] graph, boolean directed) {
		for (int i = 0; i < graph.length; i++) {
			int startIndex = directed ? 0 : i;
			String labelSource = GraphRTUtils.getLetterFromIndex(i);
			nodes.add(new GraphRTNode(labelSource, i));

			for (int j = startIndex; j < graph.length; j++) {
				// Simple graph
				if (graph[i][j]) {
					edges.add(new GraphRTEdge(i, j));
				}
			}
		}
	}

	private void updateAdjMatrixGraph(Object[][] graph, boolean directed) {
		for (int i = 0; i < graph.length; i++) {
			int startIndex = directed ? 0 : i;
			String labelSource = getVertexLabel(i);
			nodes.add(new GraphRTNode(labelSource, i));

			for (int j = startIndex; j < graph.length; j++) {

				if (graph[i][j] != null) {
					String edgeLabel = graph[i][j].toString();
					edges.add(new GraphRTEdge(i, j, edgeLabel));
				}
			}
		}
	}

	private String getVertexLabel(int index) {
		Object vertex = this.vertexList.get(index);
		String label = "";
		try {
			label = (String) nodeMethod.invoke(vertex);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return label;
	}

	private void updateAdjMatrixGraph(int[][] graph, boolean directed) {
		for (int i = 0; i < graph.length; i++) {
			int startIndex = directed ? 0 : i;
			String labelSource = GraphRTUtils.getLetterFromIndex(i);
			nodes.add(new GraphRTNode(labelSource, i));

			for (int j = startIndex; j < graph.length; j++) {
				// Weighted / Label
				if (graph[i][j] > 0) {
					edges.add(new GraphRTEdge(i, j, ((Integer)graph[i][j]).toString()));
				}
			}
		}
	}

	// Graph view - List

	private void updateAdjListGraph(GraphRT graphRT) {
		if (graphRT.isWeighted()) {
			// TODO
			//updateAdjListGraph(graphRT);
		} else {
			updateAdjListGraph((List<Integer>[]) graphRT.getGraphData(), graphRT.isDirected());
		}
	}

	private void updateAdjListGraph(List<Integer>[] graph, boolean directed) {
		for (int i = 0; i < graph.length; i++) {
			List<Integer> vertexList = graph[i];
			int startIndex = directed ? 0 : i;
			String labelSource = GraphRTUtils.getLetterFromIndex(i);
			nodes.add(new GraphRTNode(labelSource, i));

			for (int j = startIndex; j < vertexList.size(); j++) {
				edges.add(new GraphRTEdge(i, vertexList.get(j)));
			}
		}
	}

	// Nodes view - Matrix

	private void updateAdjMatrixNodes(GraphRT graphRT, int index, int level) {
		if (graphRT.isWeighted()) {
			updateAdjMatrixNodes((int[][]) graphRT.getGraphData(), graphRT.isDirected(), index, level);
		} else {
			updateAdjMatrixNodes((boolean[][]) graphRT.getGraphData(), graphRT.isDirected(), index, level);
		}
	}

	private void updateAdjMatrixNodes(int[][] graph, boolean directed, int index, int level) {
		// todo
	}

	private void updateAdjMatrixNodes(boolean[][] graph, boolean directed, int index, int level) {
		// todo
	}

	// Nodes view - List

	private void updateAdjListNodes(GraphRT graphRT, int index, int level) {
		// todo
	}

	// Plot call

	public void plot() {
		plot(new SparseLayout());
	}

	public void plot(GraphRTLayout layout) {
		updateGraph(this, layout);
	}

	// public void plot(int index, int level) {
	//	updateGraph(this, index, level);
	// }

}
