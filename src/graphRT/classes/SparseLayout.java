package graphRT.classes;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;

public class SparseLayout implements GraphRTLayout
{
    private final int nodeSize = 50;
    private final int nodeMargin = 100;
    private final int arrowSize = 10;
    private int lineSize = 2;
    private boolean directed = false;

    private Graphics graphic;
    private ArrayList<GraphRTNode> nodes;
    private ArrayList<GraphRTEdge> edges;

    public SparseLayout() {}

    public SparseLayout(Graphics graphic)
    {
        setGraphics(graphic);
    }

    public void setGraphics(Graphics graphic)
    {
        this.graphic = graphic;
    }

    public void setNodes(ArrayList<GraphRTNode> nodes)
    {
        this.nodes = nodes;
    }

    public void setEdges(ArrayList<GraphRTEdge> edges)
    {
        this.edges = edges;
    }

    public void draw(boolean directed)
    {
    	this.directed = directed;
        drawNodes(this.nodes);
        drawEdges(this.edges);
    }

    private int getEdgePosX(int size, int margin, int pos)
    {
        return size + (size / 2 + pos % lineSize * size + pos % lineSize * margin);
    }

    private int getEdgePosY(int size, int margin, int pos)
    {
        return size + (size / 2 + pos / lineSize * size + pos / lineSize * margin);
    }

    private void drawNodes(ArrayList<GraphRTNode> nodes)
    {
        for (int i = 0; i < nodes.size(); i++) {
            lineSize = (int) Math.ceil(Math.sqrt(nodes.size()));
            int offsetX = i % lineSize * nodeSize + i % lineSize * nodeMargin;
            int offsetY = i / lineSize * nodeSize + i / lineSize * nodeMargin;
            GraphRTNode node = nodes.get(i);
            // Draw circle
            this.graphic.setColor(Color.LIGHT_GRAY);
            this.graphic.fillOval(nodeSize + offsetX, nodeSize + offsetY, nodeSize, nodeSize);
            // Draw description
            this.graphic.setColor(Color.RED);
            drawCenteredString(this.graphic, node.getDescription(), new Point(nodeSize + offsetX, nodeSize + offsetY), nodeSize, new Font(Font.MONOSPACED, 0, 12));
        }
    }

    public void drawCenteredString(Graphics g, String text, Point point, int size, Font font) {
        FontMetrics metrics = g.getFontMetrics(font);
        int x = point.x + (size - metrics.stringWidth(text)) / 2;
        int y = point.y + ((size - metrics.getHeight()) / 2) + metrics.getAscent();
        g.setFont(font);
        g.drawString(text, x, y);
    }

    private void drawEdges(ArrayList<GraphRTEdge> edges)
    {
        // Draw edges
        for (int i = 0; i < edges.size(); i++) {
            this.graphic.setColor(Color.RED);
            GraphRTEdge edge = edges.get(i);
            int sourceX = getEdgePosX(nodeSize, nodeMargin, edge.getSource());
            int sourceY = getEdgePosY(nodeSize, nodeMargin, edge.getSource());
            int destinationX = getEdgePosX(nodeSize, nodeMargin, edge.getDestination());
            int destinationY = getEdgePosY(nodeSize, nodeMargin, edge.getDestination());

            drawArrow(sourceX, sourceY, destinationX, destinationY);

            int labelX = (sourceX + destinationX) / 2;
            int labelY = (sourceY + destinationY) / 2;

            this.graphic.setColor(Color.BLUE);
            this.graphic.drawString(edges.get(i).getLabel(), labelX, labelY);
        }
    }

    private void drawArrow(int sourceX, int sourceY, int destinationX, int destinationY)
    {
        int dirX = sourceX > destinationX ? 1 : sourceX < destinationX ? -1 : 0;
        int dirY = sourceY > destinationY ? 1 : sourceY < destinationY ? -1 : 0;
        boolean isDiag = Math.abs(dirY) + Math.abs(dirX) == 2;

        Point dest = new Point(destinationX, destinationY);
        Point src = new Point(sourceX, sourceY);
        int radius = isDiag ? nodeSize / 3 + 2 : nodeSize / 2 + 2;

        if (sourceX > destinationX) {
            dest.x = destinationX + radius;
            src.x = sourceX - radius;
        } else if (sourceX < destinationX) {
            dest.x = destinationX - radius;
            src.x = sourceX + radius;
        }

        if (sourceY > destinationY) {
            dest.y = destinationY + radius;
            src.y = sourceY - radius;
        } else if (sourceY < destinationY) {
            dest.y = destinationY - radius;
            src.y = sourceY + radius;
        }

        this.graphic.drawLine(src.x, src.y, dest.x, dest.y);
        if (this.directed) {
	    	Shape arrowShape = createArrowShape(src, dest);
	    	Graphics2D g2d = (Graphics2D) this.graphic;
	    	g2d.fill(arrowShape);
        }
    }

    public static Shape createArrowShape(Point fromPt, Point toPt) {
    	int scale = 6;
        Polygon arrowPolygon = new Polygon();
        arrowPolygon.addPoint(-1,1);
        arrowPolygon.addPoint(1,0);
        arrowPolygon.addPoint(-1,-1);

        double rotate = Math.atan2(toPt.y - fromPt.y, toPt.x - fromPt.x);
        AffineTransform transform = new AffineTransform();
        double xAux = toPt.x == fromPt.x ? 0 : toPt.x > fromPt.x ? -scale/2 : scale/2;
        double yAux = toPt.y == fromPt.y ? 0 : toPt.y > fromPt.y ? -scale/2 : scale/2;
        transform.translate(toPt.x + xAux, toPt.y + yAux);
        transform.scale(scale, scale);
        transform.rotate(rotate);

        return transform.createTransformedShape(arrowPolygon);
    }

    private static Point midpoint(Point p1, Point p2) {
        return new Point((int)((p1.x + p2.x)/2.0),
                         (int)((p1.y + p2.y)/2.0));
    }
}