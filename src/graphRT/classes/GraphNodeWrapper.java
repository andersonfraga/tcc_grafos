package graphRT.classes;

import java.util.LinkedList;
import java.util.List;

public class GraphNodeWrapper
{
    final public static int PADDING = -1;
    final public static int MAX_LAYERS = 200;
    final public static int MAX_SWEEPS = 35;

    int index = MAX_SWEEPS;
    public int layer;

    public final GraphRTNode node;
    private final List<GraphNodeWrapper> pred = new LinkedList<>();
    private final List<GraphNodeWrapper> succ = new LinkedList<>();

    GraphNodeWrapper(GraphRTNode n, int l)
    {
        node = n;
        layer = l;
    }

    GraphNodeWrapper(int l)
    {
        this(new GraphRTNode(), l);
    }

    public List<GraphNodeWrapper> getPredecessors()
    {
        return pred;
    }

    public List<GraphNodeWrapper> getSuccessors() {
        return succ;
    }

    void addPredecessor(GraphNodeWrapper node)
    {
        pred.add(node);
    }

    void addSuccessor(GraphNodeWrapper node)
    {
        succ.add(node);
    }

    boolean isDummy()
    {
        return (node.isDummy() && (layer != PADDING));
    }

    boolean isPadding()
    {
        return (node.isDummy() && (layer == PADDING));
    }

    int getBaryCenter(List<GraphNodeWrapper> list)
    {
        if (list.isEmpty())
            return (this.index);

        if (list.size() == 1)
            return (list.get(0).index);

        double barycenter = 0;

        for (GraphNodeWrapper node : list)
            barycenter += node.index;

        return ((int) (barycenter / list.size()));
    }

    int getPriorityDown()
    {
        if (isPadding())
            return (0);
        if (isDummy()) {
            if (this.succ.size() > 0 && this.succ.get(0).isDummy())
                return (Integer.MAX_VALUE); // part of a straight line
            else
                return (Integer.MAX_VALUE >> 1); // start of a straight line
        }
        return (this.pred.size());
    }

    int getPriorityUp()
    {
        if (isPadding())
            return (0);

        if (isDummy()) {
            if (this.pred.size() > 0 && this.pred.get(0).isDummy())
                return (Integer.MAX_VALUE); // part of a straight line
            else
                return (Integer.MAX_VALUE >> 1); // start of a straight line
        }
        return (this.succ.size());
    }

    public String toString()
    {
        String ret = "Wrapper " + (node.isDummy() ? "dummy" : node.getDescription())
            + " (";

        String content = "";

        for (GraphNodeWrapper nw : pred) {
            content += (content.isEmpty() ? "" : ", ") + nw.node.getDescription();
        }

        ret += content + " - ";

        content = "";

        for (GraphNodeWrapper nw : succ) {
            content += (content.isEmpty() ? "" : ", ") + nw.node.getDescription();
        }

        return ret + content + ")";
    }
}
