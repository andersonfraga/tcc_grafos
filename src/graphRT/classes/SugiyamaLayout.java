
package graphRT.classes;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.*;
import java.util.List;

/**
 * Algoritmo implementado a partir do disposto em
 * http://www.javased.com/index.php?source_dir=zest/org.eclipse.zest.layouts/src/org/eclipse/zest/layouts/algorithms/SugiyamaLayoutAlgorithm.java
 *
 * Copyrights reservados
 */
public class SugiyamaLayout implements GraphRTLayout
{
    // Tree direction constants
    final public static int HORIZONTAL = 1;
    final public static int VERTICAL = 2;

    final public static int SIZE_NODE = 50;

    // Internal constants
    private class Position
    {
        private final Integer x;
        private final Integer y;

        Position(Integer x, Integer y)
        {
            this.x = x;
            this.y = y;
        }

        public Integer getX()
        {
            return this.x;
        }

        public Integer getY()
        {
            return this.y;
        }

        public String toString()
        {
            return getX() + ":" + getY();
        }
    }

    private int direction = 2;

    private boolean directed;

    private List<ArrayList<GraphNodeWrapper>> layers = new ArrayList<>(GraphNodeWrapper.MAX_LAYERS);

    private Map<GraphRTNode, GraphNodeWrapper> map = new IdentityHashMap<>();

    private Map<GraphRTNode, Position> locations = new HashMap<>();

    private int last;

    private Graphics graphic;
    private ArrayList<GraphRTNode> nodes;
    private ArrayList<GraphRTEdge> edges;

    public SugiyamaLayout() {}

    public SugiyamaLayout(Graphics graphic)
    {
        setGraphics(graphic);
    }

    public void setGraphics(Graphics graphic)
    {
        this.graphic = graphic;
    }

    public void setNodes(ArrayList<GraphRTNode> nodes)
    {
        this.nodes = nodes;
    }

    public void setEdges(ArrayList<GraphRTEdge> edges)
    {
        this.edges = edges;
    }

    public void draw(boolean directed)
    {
        if (this.nodes.isEmpty()) {
            this.edges.forEach(edge -> {
                if (!this.nodes.contains(edge)) {
                    this.nodes.add(new GraphRTNode(edge.getLabel(), edge.getSource()));
                }
            });
        }

        System.out.println(this.nodes.size());
        System.out.println(this.edges.size());

    	this.directed = directed;

        layers.clear();
        map.clear();

        createLayers();

        padLayers();

        // reduce and refine iteratively, depending on the depth of the graph
        for (int i = 0; i < layers.size(); i++) {
            reduceCrossings();
            refineLayers();
        }

        reduceCrossings();
        calculatePositions();

        printNodes();
    }

    private void printNodes()
    {
        locations.forEach((k, v) -> {
            GraphRTNode node = k;
            Position pos = v;

            if (!node.isDummy()) {
                this.graphic.setColor(Color.GRAY);
                this.graphic.fillOval(pos.x, pos.y, SIZE_NODE, SIZE_NODE);

                // Draw description
                this.graphic.setColor(Color.RED);
                drawCenteredString(this.graphic, node.getDescription(), pos, SIZE_NODE);
            }
        });

        layers.forEach(nodew -> {
            nodew.forEach(wrapper -> {
                wrapper.getSuccessors().forEach(succ -> {
                    Position node_src = locations.get(wrapper.node);
                    Position node_dest = locations.get(succ.node);

                    this.graphic.setColor(Color.BLACK);
                    this.drawArrow(
                            node_src.getX(),
                            node_src.getY(),
                            node_dest.getX(),
                            node_dest.getY(),
                            !wrapper.node.isDummy(),
                            !succ.node.isDummy()
                    );
                });
            });
        });

        for (Map.Entry<GraphRTNode, GraphNodeWrapper> item : map.entrySet()) {
            List<GraphRTNode> list = new ArrayList<>();

            getNodeSucc(item.getValue(), list);

            int counter_sec = 0;
            int counter_ger = 0;
            int position_elm = 0;

            for (GraphRTNode n : list) {
                GraphRTNode child;

                if (n.isDummy()) {
                    counter_sec++;
                }
                else {
                    if (counter_sec == 0) {
                        child = list.get(counter_ger);
                    }
                    else {
                        position_elm = counter_ger - Math.round(counter_sec / 2) - 1;
                        child = list.get(position_elm);
                    }

                    counter_sec = 0;

                    String label = getEdgeItem(item.getKey().getPosGraph(), n.getPosGraph());

                    this.graphic.setColor(Color.BLUE);
                    this.graphic.drawString(label, locations.get(child).getX() + 35, locations.get(child).getY());
                    System.out.println(item.getKey().getDescription() + " " + n + ": " + label);
                }

                counter_ger++;
            }


        }
    }

    private void getNodeSucc(GraphNodeWrapper wrappers, List<GraphRTNode> list)
    {
        for (GraphNodeWrapper wrapper : wrappers.getSuccessors()) {
            list.add(wrapper.node);

            if (wrapper.isDummy()) {
                getNodeSucc(wrapper, list);
            }
        }
    }

    private String getEdgeItem(int source, int destination)
    {
        for (GraphRTEdge edge : edges) {
            if (edge.getSource() == source && edge.getDestination() == destination) {
                return edge.getLabel();
            }
        }

        return null;
    }

    public void drawCenteredString(Graphics g, String text, Position position, int size)
    {
        Font font = new Font(Font.MONOSPACED, 0, 12);
        FontMetrics metrics = g.getFontMetrics(font);
        int x = position.x + (size - metrics.stringWidth(text)) / 2;
        int y = position.y + ((size - metrics.getHeight()) / 2) + metrics.getAscent();
        g.setFont(font);
        this.graphic.setColor(Color.BLACK);
        g.drawString(text, x, y);
        g.drawString(text, x-2, y);
        g.drawString(text, x, y-2);
        g.drawString(text, x-2, y-2);
        this.graphic.setColor(Color.WHITE);
        g.drawString(text, x-1, y-1);
    }


    private void drawArrow(int sourceX, int sourceY, int destinationX, int destinationY, boolean da_src, boolean da_dest)
    {

        int dir_x = sourceX > destinationX ? 1 : sourceX < destinationX ? -1 : 0;
        int dir_y = sourceY > destinationY ? 1 : sourceY < destinationY ? -1 : 0;
        boolean is_diagonal = Math.abs(dir_y) + Math.abs(dir_x) == 2;

        Point dest = new Point(destinationX, destinationY);
        Point src = new Point(sourceX, sourceY);
        int radius = SIZE_NODE / 2;

        src.x += da_src ? (is_diagonal ? (dir_x == 1 ? 6 : 44) : radius) : radius;
        src.y += da_src ? (is_diagonal ? (dir_y == 1 ? 6 : 44) : SIZE_NODE) : radius;
        dest.x += da_dest ? (is_diagonal ? (dir_x == -1 ? 6 : 44) : radius) : radius;
        dest.y += da_dest ? (is_diagonal ? (dir_y == -1 ? 6 : 44) : 0) : radius;

        this.graphic.drawLine(src.x, src.y, dest.x, dest.y);

        if (this.directed && da_dest) {
            Shape arrowShape = createArrowShape(src, dest);
            Graphics2D g2d = (Graphics2D) this.graphic;
            g2d.fill(arrowShape);
        }
    }

    public static Shape createArrowShape(Point fromPt, Point toPt) {
        int scale = 6;
        Polygon arrowPolygon = new Polygon();
        arrowPolygon.addPoint(-1,1);
        arrowPolygon.addPoint(1,0);
        arrowPolygon.addPoint(-1,-1);

        double rotate = Math.atan2(toPt.y - fromPt.y, toPt.x - fromPt.x);
        AffineTransform transform = new AffineTransform();
        double xAux = toPt.x == fromPt.x ? 0 : toPt.x > fromPt.x ? -scale/2 : scale/2;
        double yAux = toPt.y == fromPt.y ? 0 : toPt.y > fromPt.y ? -scale/2 : scale/2;
        transform.translate(toPt.x + xAux, toPt.y + yAux);
        transform.scale(scale, scale);
        transform.rotate(rotate);

        return transform.createTransformedShape(arrowPolygon);
    }

    protected void createLayers()
    {
        List<GraphRTNode> nodes = (List<GraphRTNode>) this.nodes.clone();
        List<GraphRTEdge> edges = (List<GraphRTEdge>) this.edges.clone();

        List<GraphRTNode> predecessors = findRoots(nodes, edges);
        nodes.removeAll(predecessors);
        addLayer(predecessors);

        for (int level = 1; nodes.isEmpty() == false; level++) {
            if (level >= GraphNodeWrapper.MAX_LAYERS) {
                return;
            }

            List<GraphRTNode> layer = new ArrayList<>();

            for (GraphRTNode _node : nodes) {
                List<GraphRTNode> node_preds = findPredecessors(_node);

                if (predecessors.containsAll(node_preds)) {
                    layer.add(_node);
                }
            }

            nodes.removeAll(layer);
            predecessors.addAll(layer);
            addLayer(layer, level);
        }
    }

    protected void addLayer(List<GraphRTNode> list)
    {
        addLayer(list, 0);
    }

    protected void addLayer(List<GraphRTNode> list, int actual_level)
    {
        ArrayList<GraphNodeWrapper> layer = new ArrayList<>(list.size());

        for (GraphRTNode _node : list) {
            GraphNodeWrapper nw = new GraphNodeWrapper(_node, actual_level);
            map.put(_node, nw);
            layer.add(nw);

            for (GraphRTNode predecessor : findPredecessors(_node)) {
                GraphNodeWrapper wrapper_pred = map.get(predecessor);

                for (int dummy_level = wrapper_pred.layer + 1; dummy_level < nw.layer; dummy_level++) {
                    GraphNodeWrapper dummy = new GraphNodeWrapper(dummy_level);
                    dummy.addPredecessor(wrapper_pred);
                    wrapper_pred.addSuccessor(dummy);
                    wrapper_pred = dummy;

                    layers.get(dummy_level).add(dummy);
                }

                nw.addPredecessor(wrapper_pred);
                wrapper_pred.addSuccessor(nw);
            }
        }

        layers.add(layer);
        updateIndex(layer);
    }

    protected void padLayers()
    {
        this.last = 0;

        for (List<GraphNodeWrapper> it : this.layers)
            if (it.size() > this.last)
                this.last = it.size();

        this.last--;

        for (List<GraphNodeWrapper> it : this.layers) {
            for (int i = it.size(); i <= this.last; i++)
                it.add(new GraphNodeWrapper(i));

            updateIndex(it);
        }
    }

    protected void reduceCrossings()
    {
        for (int round = 0; round < GraphNodeWrapper.MAX_SWEEPS; round++) {
            if ((round & 1) == 0) { // if round is even then do a bottom-up scan
                for (int idx = 1; idx < layers.size(); idx++)
                    reduceCrossingsDown(layers.get(idx));
            } else { // else top-down
                for (int idx = layers.size() - 2; idx >= 0; idx--)
                    reduceCrossingsUp(layers.get(idx));
            }
        }
    }

    private void reduceCrossingsUp(ArrayList<GraphNodeWrapper> layer)
    {
        for (GraphNodeWrapper node : layer)
            node.index = node.getBaryCenter(node.getSuccessors());

        Collections.sort(layer, Comparator.comparingInt(node -> node.index));

        updateIndex(layer);
    }

    private void reduceCrossingsDown(ArrayList<GraphNodeWrapper> layer)
    {
        for (GraphNodeWrapper node : layer)
            node.index = node.getBaryCenter(node.getPredecessors());

        Collections.sort(layer, Comparator.comparingInt(node -> node.index));

        updateIndex(layer);
    }

    protected void refineLayers()
    {
        for (int idx = 1; idx < layers.size(); idx++)
            refineLayersDown(layers.get(idx));

        for (int idx = layers.size() - 2; idx >= 0; idx--)
            refineLayersUp(layers.get(idx));

        for (int idx = 1; idx < layers.size(); idx++)
            refineLayersDown(layers.get(idx));
    }

    private void refineLayersUp(ArrayList<GraphNodeWrapper> layer)
    {
        List<GraphNodeWrapper> list = new ArrayList<>(layer);

        Collections.sort(list, (node1, node2) -> {
            // descending
            return (node2.getPriorityUp() - node1.getPriorityUp());
        });

        for (GraphNodeWrapper it : list) {
            if (it.isPadding())
                break;

            int delta = it.getBaryCenter(it.getPredecessors()) - it.index;

            for (int i = 0; i < delta; i++)
                layer.add(it.index, layer.remove(this.last));
        }
        updateIndex(layer);
    }

    private void refineLayersDown(ArrayList<GraphNodeWrapper> layer)
    {
        List<GraphNodeWrapper> list = new ArrayList<>(layer);

        Collections.sort(list, (node1, node2) -> {
            // descending
            return (node2.getPriorityDown() - node1.getPriorityDown());
        });

        for (GraphNodeWrapper it : list) {
            if (it.isPadding())
                break;

            int delta = it.getBaryCenter(it.getSuccessors()) - it.index;

            for (int i = 0; i < delta; i++)
                layer.add(it.index, layer.remove(this.last));
        }

        updateIndex(layer);
    }

    protected void calculatePositions()
    {
        int dx = 80; //500 / layers.size();
        int dy = 80; //500 / (this.last + 1);

        if (direction == HORIZONTAL) {
            for (ArrayList<GraphNodeWrapper> layer: this.layers) {
                for (GraphNodeWrapper node_wrapper : layer) {
                    GraphRTNode node = node_wrapper.node;

                    if (node == null) {
                        node = new GraphRTNode();
                    }

                    Position pos = new Position((node_wrapper.layer * dx) + dx, (node_wrapper.index * dy) + dy);
                    locations.put(node, pos);
                }
            }
        }
        else {

            for (ArrayList<GraphNodeWrapper> layer: this.layers) {
                for (GraphNodeWrapper node_wrapper : layer) {
                    GraphRTNode node = node_wrapper.node;

                    if (node == null) {
                        node = new GraphRTNode();
                    }

                    Position pos = new Position((node_wrapper.index * dx) + dx, (node_wrapper.layer * dy) + dy);
                    locations.put(node, pos);
                }

            }
        }
    }

    private List<GraphRTNode> findPredecessors(GraphRTNode node)
    {
        return findPredecessors(node, this.nodes, this.edges);
    }

    private List<GraphRTNode> findPredecessors(GraphRTNode node, List<GraphRTNode> nodes, List<GraphRTEdge> edges)
    {
        List<GraphRTNode> preds = new ArrayList<>();

        for (GraphRTNode n : nodes) {
            for (GraphRTEdge e : edges) {
                if (n.getPosGraph() == e.getSource() && node.getPosGraph() == e.getDestination()) {
                    preds.add(n);
                }
            }
        }

        return preds;
    }

    private List<GraphRTNode> findRoots()
    {
        return findRoots(this.nodes, this.edges);
    }

    private List<GraphRTNode> findRoots(List<GraphRTNode> nodes, List<GraphRTEdge> edges)
    {
        List<GraphRTNode> roots = new ArrayList<>();

        // no predecessors means: this is a root, add it to list

        for (GraphRTNode _node : nodes) {
            boolean is_root = true;

            for (GraphRTEdge _edge : edges) {
                if (_edge.getDestination() == _node.getPosGraph()) {
                    is_root = false;
                }
            }

            if (is_root) {
                roots.add(_node);
            }
        }

        if (roots.isEmpty()) {
            roots.add(nodes.get(0));
        }

        return roots;
    }

    private static void updateIndex(List<GraphNodeWrapper> list) {
        for (int idx = 0; idx < list.size(); idx++)
            list.get(idx).index = idx;
    }

}