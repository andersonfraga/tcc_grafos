
package graphRT.classes;

import java.awt.Graphics;
import java.util.ArrayList;

interface GraphRTLayout
{
    void setGraphics(Graphics graphic);
    void setNodes(ArrayList<GraphRTNode> nodes);
    void setEdges(ArrayList<GraphRTEdge> edges);
    void draw(boolean directed);
}