package graphRT.classes;

public class GraphRTNode
{
	private boolean is_dummy = false;
	private String description;
	private String label;
	private String color;
	private int pos_graph;
	private int layer;

	public GraphRTNode() {
		is_dummy = true;
	}

	public GraphRTNode(String description, int pos_graph)
	{
		this(description, null, null, pos_graph);
	}

	public GraphRTNode(String description, String label, String color, int pos_graph)
	{
		this.description = description;
		this.label = label;
		this.color = color;
		this.pos_graph = pos_graph;
	}

	public boolean isDummy() {
		return is_dummy;
	}

	public String getDescription()
	{
		return this.description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getLabel()
	{
		return this.label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}

	public String getColor()
	{
		return this.color;
	}

	public void setColor(String color)
	{
		this.color = color;
	}

	public int getPosGraph()
	{
		return this.pos_graph;
	}

	public void setLayer(int layer)
	{
		this.layer = layer;
	}

	public int getLayer()
	{
		return this.layer;
	}

	public String toString() {
		return this.getDescription();
	}
}
