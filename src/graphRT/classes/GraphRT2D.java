package graphRT.classes;

import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JFrame;

public class GraphRT2D extends JFrame
{
	GraphRTLayout layout;

	public GraphRT2D()
	{
		this(new SugiyamaLayout());
		//this(new SparseLayout());
	}

	public GraphRT2D(GraphRTLayout layout)
	{
		this(layout, 1000, 1000);
	}

	public GraphRT2D(GraphRTLayout layout, int width, int height)
	{
		this.layout = layout;

		setTitle("GraphRT");
		setSize(width, height);
		setVisible(true);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	protected void paint(Graphics g, ArrayList<GraphRTNode> nodes, ArrayList<GraphRTEdge> edges, boolean directed)
	{
		layout.setGraphics(g);
		layout.setNodes(nodes);
		layout.setEdges(edges);

		layout.draw(directed);
	}

	public void updatePaint(ArrayList<GraphRTNode> nodes, ArrayList<GraphRTEdge> edges, boolean directed)
	{
		paint(this.getGraphics(), nodes, edges, directed);
	}
}
