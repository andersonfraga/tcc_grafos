package graphRT.classes;

public class GraphRTEdge
{
	private int source;
	private int destination;
	private String label;

	public GraphRTEdge() {}

	public GraphRTEdge(int source, int destination)
	{
		this(source, destination, "");
	}

	public GraphRTEdge(int source, int destination, String label)
	{
		this.source = source;
		this.destination = destination;
		this.label = label;
	}

	public int getSource()
	{
		return source;
	}

	public void setSource(int source)
	{
		this.source = source;
	}

	public int getDestination()
	{
		return destination;
	}

	public void setDestination(int destination)
	{
		this.destination = destination;
	}

	public String getLabel()
	{
		return label;
	}

	public void setLabel(String label)
	{
		this.label = label;
	}
}
